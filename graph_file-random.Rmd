

```{r data generation process, include = FALSE}
library(exams)
options(scipen = 999) #prevent scientific notation 

a=1
b=0.5
x <- runif(1000, min=100, max=200)
y <- a + b * x + rnorm(1000, 10, 7)

Cor <- cor(x,y)
Inc <- coef(lm(y~x))[2]

write.csv(data.frame(x,y), file="dados.csv", quote = FALSE, row.names = FALSE)

```



Question
========
Os dados do arquivo [dados.csv](dados.csv) foram utilizados para construir o seguinte gráfico de dispersão:

```{r dispersao, echo = FALSE, results = "hide", fig.cap = " "}
plot(y~x)
```

Responda (com três casas decimais):


Questionlist
------------
* Qual o coeficiente de correlação linear?
* Qual o coeficiente de inclinação da reta linear ajustada?


Solution
========
```{r solution, echo = FALSE, results = "hide", fig.cap = ""}
plot(y~x)
abline(lm(y~x), col="red")
mtext(paste("r =",fmt(Cor,3), " b =", fmt(Inc,3)), 3, line=-2)



```

Solutionlist
------------
* O coeficiente de correlação linear é `r fmt(Cor, 3)`
* O coeficiente de inclinação da reta linear ajustada é `r fmt(Inc,3)`

Meta-information
================
exname: grafico e arquivo
extype: cloze
exsolution: `r fmt(Cor,3)`|`r fmt(Inc,3)`
exclozetype: num|num
extol: 0.01
exshuffle: TRUE