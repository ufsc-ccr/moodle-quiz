
Question
========
Qual das seguintes afirmativas está correta?

Answerlist
----------
* A bandeira do Brasil tem a cor verde.
* O idioma oficial do Brasil é o brasileiro.
* A moeda atual do Brasil é o cruzeiro.
* O Brasil está na América do Norte.
* A capital do Brasil é Buenos Aires.
* O Brasil tem 27 estados.
* Dom Pedro I foi o primeiro presidente do Brasil.


Solution
========

Answerlist
----------
* CORRETA. 
* FALSA. O idioma oficial do Brasil é o português.
* FALSA. A moeda atual do Brasil é o Real.
* FALSA. O Brasil está na América do Sul.
* FALSA. A capital do Brasil é Brasília.
* FALSA. O Brasil tem 27 Unidades Federativas. São 26 estados mais o Distrito Federal
* FALSA. O primeiro presidente foi Deodoro da Fonseca.


Meta-information
================
exname: Brasil
extype: schoice
exsolution: 1000000
exshuffle: 5
