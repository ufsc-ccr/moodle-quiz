
Question
========
Marque todas as alternativas corretas.
  
Answerlist
----------
* O céu é azul.
* 2 + 2 = 4
* A Austrália é um continente.
* Na China se fala chinês.


Solution
========

Answerlist
----------
* CORRETA.
* CORRETA.
* FALSA. A Austrália é um país.
* FALSA. Na China se fala Mandarim.


Meta-information
================
exname: assuntos
extype: mchoice
exsolution: 1100
exshuffle: TRUE

